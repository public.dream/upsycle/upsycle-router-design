; Message types with their IDs
message =
  [ 0, UNICAST_HEADER, BODY, SIG, ?VIA ] /
  [ 1, MULTICAST_HEADER, BODY, SIG, ?VIA ]

; Unicast message
UNICAST_HEADER = {
  source,
  destination,
  ttl,
  expiry
}

; Multicast message
MULTICAST_HEADER = {
  group,
  ttl,
  expiry,
  seen
}

; Message body
BODY = bstr

; Signature over the header and body
SIG = sig

; Last hop public key the message arrived via
VIA = pubkey

; Source public key address
source = (src: pubkey)

; Destination public key address
destination = (dst: pubkey)

; Group public key address
group = (grp: pubkey)

; Time-to-live: duration the message can be queued relative to its arrival, in seconds
ttl = (ttl: uint .size 32)

; Expiry time: absolute time the message expires at and should be deleted from queues,
;              in minutes since 2020-01-01 00:00 UTC
expiry = (exp: uint .size 32)

; List of message IDs recently seen by the sender
seen = ( sn: [ 0*16 msg-id ])


; Message ID: BLAKE3 hash over the message type, header, body, and signature.
msg-id = hash

; 256-bit BLAKE3 hash
hash = bytes .size 32

; Curve25519 public key
pubkey = bytes .size 32

; EdDSA signature over the message type, header, and body.
sig = bytes .size 64
